#include "Gameplay.h"
#include <iostream>
#include <random>

Gameplay::Gameplay()
{
}

Gameplay::~Gameplay()
{
	release();
}

void Gameplay::updateCamera()
{
}

bool Gameplay::init()
{
	cameraX = 1366;
	cameraY = 768;
	camV = 1000.f;
	//camera.reset(sf::FloatRect(0.f, 0.f, 1366, 768)); init kamery
	camera.reset(sf::FloatRect(-683.f, -384.f, 1366, 768));

	mCircle.setFillColor(sf::Color(255, 255, 0));
	aim.setFillColor(sf::Color(0, 0, 255));
	aim.setRadius(5.f);
	mouseButton.setFillColor(sf::Color(0, 192, 192));
	mouseButton.setRadius(15.f);
	mouseButton.setPosition(0.f, 0.f);
	rldIndicator.setRadius(25.f);
	rldIndicator.setFillColor(sf::Color(0, 192, 192));
	rldIndicator.setOutlineColor(sf::Color(0, 0,0));
	rldIndicator.setPosition(0., 0.);
	rldIndicator.setOutlineThickness(3.f);



	mVelocity.x = 0;
	mVelocity.y = 0;
	mCircle.setPosition(0, 0);
	mCircle.setRadius(50.f);
	mCircle.setOutlineColor(sf::Color(0, 0, 0));
	mCircle.setOutlineThickness(2.f);

	// ustawianie srodka odniesienia na srodku obiektu a nei w roku
	mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());
	
	mBox.setFillColor(sf::Color(0, 0, 0, 255));
	mBox.setSize(sf::Vector2f(10, 10));

	mBox.setPosition(0.f, 0.f);
	
	mSpeed = 300.f;

	mCamVelocity.x = 0;			
	mCamVelocity.y = 0;			

	// colors
	circleColor.r = 255;
	circleColor.g = 0;
	circleColor.b = 0;
	circleColor.a = 255;

	backgroundColor.r = 0;
	backgroundColor.g = 255;
	backgroundColor.b = 0;
	backgroundColor.a = 255;

	enemyColor.r = 255;
	enemyColor.g = 0;
	enemyColor.b = 0;
	enemyColor.a = 255;

	deadColor = backgroundColor;
	deadColor.a = 0;

	isKeyPressedLock = false;


	// generowanie 100 enemiesow
	for (int i = 0; i < 100; ++i) {
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_int_distribution<> dist1(0, 1366);
		std::uniform_int_distribution<> dist2(0, 768);

		tabEnemy[i].enemy.setPosition(dist1(mt), dist2(mt));
		tabEnemy[i].enemy.setRadius(10.f);
		tabEnemy[i].enemy.setFillColor(enemyColor);
		tabEnemy[i].enemy.setOrigin(tabEnemy[i].enemy.getRadius(), tabEnemy[i].enemy.getRadius());
		
		/*tabCircle[i].setPosition(dist1(mt), dist2(mt));
		tabCircle[i].setRadius(10.f);
		tabCircle[i].setFillColor(enemyColor);
		tabCircle[i].setOrigin(tabCircle[i].getRadius(), tabCircle[i].getRadius());
		*/
	}
	return true;
}
// dt to czas trwania klatki
bool Gameplay::update(float dt, sf::RenderWindow &window, sf::Event event)
{
	updateCamera();
	mVelocity.x = 0;
	mVelocity.y = 0;

	mCamVelocity.x = 0;
	mCamVelocity.y = 0;


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		mVelocity.y -= mSpeed;
		mCamVelocity.y -= mSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		mVelocity.y += mSpeed;
		mCamVelocity.y += mSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		mVelocity.x -= mSpeed;
		mCamVelocity.x -= mSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
		mVelocity.x += mSpeed;
		mCamVelocity.x += mSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		mCamVelocity.y -= mSpeed;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		mCamVelocity.y += mSpeed;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::J))
		mCamVelocity.x -= mSpeed;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		mCamVelocity.x += mSpeed;
/*
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P)) {
		cameraX *= 1.20;
		cameraY *= 1.20;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::O)) {
		cameraX *= 0.60;
		cameraY *= 0.60;
	}
*/
	camera.setSize(cameraX, cameraY);
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
		if (mCircle.getRadius() > 20.f) {
			std::random_device rd;
			std::mt19937 mt(rd());
			std::uniform_int_distribution<> dist(0, 255);

			circleColor.r = dist(mt);
			circleColor.g = dist(mt);
			circleColor.b = dist(mt);
			circleColor.a = 255;
		
			mCircle.setFillColor(circleColor);
			mSpeed = 8000.f;
		
			mCircle.setRadius(mCircle.getRadius() - 0.5f);
			mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());	// zmiana srodka obliczanie kolizji kola
		}
		else {
			circleColor.r = 255;
			circleColor.g = 255;
			circleColor.b = 0;
			circleColor.a = 255;
			mSpeed = 300.f;

			mCircle.setFillColor(circleColor);
		}
	}
	else {
		circleColor.r = 255;
		circleColor.g = 255;
		circleColor.b = 0;
		circleColor.a = 255;
		mSpeed = 300.f;

		mCircle.setFillColor(circleColor);
	}
	//hax!
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::N)) {
		
			mCircle.setRadius(mCircle.getRadius() + 5.f);
			mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());	// zmiana srodka obliczanie kolizji kola
	}

	if ((mVelocity.x != 0) && (mVelocity.y != 0)) {
		mVelocity *= InvSqrt2;
	}
	if ((mCamVelocity.x != 0) && (mCamVelocity.y != 0)) {
		mCamVelocity *= InvSqrt2;
	}
	//sf::Mouse::setPosition(sf::Vector2i(100, 200), window);

	
	// do myszki trzeba przetransortowac window z maina DONE! after 2weeks :)
	mCircle.move(mVelocity*dt);





	//-----------------------------------------------------------------------------------------------------CELOWNIK

	

	// while there are pending events...
	while (window.pollEvent(event))
	{/*
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Space)
			{
				std::cout << "the escape key was pressed" << std::endl;
				std::cout << "control:" << event.key.control << std::endl;
				std::cout << "alt:" << event.key.alt << std::endl;
				std::cout << "shift:" << event.key.shift << std::endl;
				std::cout << "system:" << event.key.system << std::endl;
				std::cout << "left click " << cntr << std::endl;
				cntr++;
				isKeyPressedLock = true;
				mouseButton.setPosition(aim.getPosition().x, aim.getPosition().y);
			}



		}

		else if (event.type == sf::Event::MouseButtonPressed)
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				std::cout << "the right button was pressed" << std::endl;
				std::cout << "mouse x: " << event.mouseButton.x << std::endl;
				std::cout << "mouse y: " << event.mouseButton.y << std::endl;
			}
		}*/
		switch (event.type)
		{
			// window closed
		case sf::Event::Closed:
			window.close();
			break;

			// key pressed
		case sf::Event::KeyPressed:
			std::cout << "key prs" << std::endl;
				break;

			// we don't process other types of events
		default:
			break;
		}
	}
		
		/*

		// check the type of the event...
		switch (event.type)
		{
			// window closed
		case sf::Event::Closed:
			window.close();
			break;

			// key pressed
		case sf::Event::KeyPressed:
			...
				break;

			// we don't process other types of events
		default:
			break;
		}
		*/
	


	sf::Vector2i mousePosition = sf::Mouse::getPosition( window);
	//aim.setPosition(sf::Vector2f(mousePosition.x - camera.getCenter().x, mousePosition.y - camera.getCenter().y));
	aim.setPosition(sf::Vector2f(mousePosition.x + mCircle.getPosition().x - 683, mousePosition.y + mCircle.getPosition().y - 384));
	

	// dodac reloada
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		// left click...
		if (!isKeyPressedLock && reload >= 50 && mCircle.getRadius() > 20.f) {
			reload = 0;
			std::cout << "left click " << cntr << std::endl;
			cntr++;
			isKeyPressedLock = true;
			mouseButton.setPosition(aim.getPosition().x, aim.getPosition().y);
			rldIndicator.setFillColor(sf::Color(0, 0, 0));
			mCircle.setRadius(mCircle.getRadius() - 10.5f);
			mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());	// zmiana srodka obliczanie kolizji kola
		}
	}
	else {
		isKeyPressedLock = false;
	}
	if (reload < 50) {
		reload++;
		std::cout << reload;
		rldIndicator.setFillColor(sf::Color(0, 192 / 50 * reload, 192 / 50 * reload));
		if (reload >= 50)
			rldIndicator.setFillColor(sf::Color(255, 0, 0));
	}
	//-----------------------------------------------------------------------------------------------------CELOWNIK END



	//aim.setPosition((sf::Vector2f)(sf::Mouse::getPosition(window)));
	//mVelocity.x = mousePosition.x;
	//mVelocity.y = mousePosition.y;
	//mCircle.setPosition(mVelocity);
	
	//mCircle.move(mVelocity*dt);
	
	camera.move(mCamVelocity*dt);
	rldIndicator.setPosition(camera.getCenter().x - 683, camera.getCenter().y - 384);
	for (int i = 0; i < 100; ++i) {
	//	tabCircle[i].setFillColor(circleColor);
		if(!tabEnemy[i].isDead)
			if (circleColision(tabEnemy[i].enemy, mCircle)) {
				tabEnemy[i].enemy.setFillColor(deadColor);
				tabEnemy[i].isDead = true;
				mCircle.setRadius(mCircle.getRadius() + 1.5f);					
				mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());	// zmiana srodka obliczanie kolizji kola
			}
	}
	


	return true;
}

void Gameplay::draw(sf::RenderWindow &window)
{
	
	//camera.reset(sf::FloatRect(-50, -25, camV * (float) window.getSize().x  / window.getSize().y, camV));
	camera.setSize(camV * (float)window.getSize().x / window.getSize().y, camV);
	
	window.setView(camera);
	
	window.clear(sf::Color(0, 255, 0));


	window.draw(mCircle);

	window.draw(mBox);
	window.draw(aim);
	window.draw(mouseButton);
	window.draw(rldIndicator);
	// rysuj tylko zywych przeciwnikow
	for (int i = 0; i < 100; ++i) {
		if(!tabEnemy[i].isDead)
			window.draw(tabEnemy[i].enemy);
	}

	


	window.setView(window.getDefaultView());
}

void Gameplay::release()
{
}

bool Gameplay::circleColision(sf::CircleShape c1, sf::CircleShape c2) {
	// odleglosc mniejszza od dwoch promieni
	// return 1 if sqrt(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2)) <= c1.r + c2.r;

	//	return sqrt(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2)) <= c1.r + c2.r;

	//or bo sqrt zu�ywa sporo zasob�w
	//		(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2)) <= (c1.r + c2.r) * (c1.r + c2.r);
	float radius = (c1.getRadius() + c2.getRadius()) * (c1.getRadius() + c2.getRadius());
	if ((pow(c2.getPosition().x - c1.getPosition().x, 2) + pow( c2.getPosition().y - c1.getPosition().y, 2)) <= radius)
		return 1;
	else
		return 0;

}