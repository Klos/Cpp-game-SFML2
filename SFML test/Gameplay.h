#pragma once

#include "LibsAndDeclarations.h"
#include "GameStage.h"
#include "Player.h"
#include "Enemy.h"

class Gameplay : public GameStage
{
public:
	Gameplay();
	Gameplay(const Gameplay&) = delete;
	Gameplay& operator=(const Gameplay&) = delete;
	virtual ~Gameplay();

private:
	sf::View camera;
	int cameraX;
	int cameraY;
	sf::RectangleShape mBox;
	float camV;
	float cameraOldX;
	float cameraOldY;

	void updateCamera();
	sf::CircleShape mCircle;

	sf::CircleShape aim;

	sf::Vector2f mVelocity;
	sf::Vector2f mCamVelocity;
	sf::Color circleColor;
	sf::Color enemyColor;
	sf::Color backgroundColor;
	sf::Color deadColor;
	float mSpeed;


	// mouse pressed
	bool isKeyPressedLock;
	int cntr = 0;
	sf::CircleShape mouseButton;
	//sf::Event event;
	int reload = 10;
	sf::CircleShape rldIndicator;



	const float InvSqrt2 = 0.70710678118f;

	sf::CircleShape tabCircle[100];	// zamieniam to na enemy, �eby mie� isDead
	Enemy tabEnemy[100];
	sf::Vector2f tabVector[100];

public:
	virtual bool init();
	virtual bool update(float dt, sf::RenderWindow &window, sf::Event event);
	virtual void draw(sf::RenderWindow &window);
	virtual void release();

	// Dopisz tutaj w parametrach consty
	bool circleColision(sf::CircleShape c1, sf::CircleShape c2);
		// odleglosc mniejszza od dwoch promieni
		// return 1 if sqrt(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2)) <= c1.r + c2.r;

	//	return sqrt(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2)) <= c1.r + c2.r;

		//or bo sqrt zu�ywa sporo zasob�w
	//		(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2)) <= (c1.r + c2.r) * (c1.r + c2.r);

	
};
