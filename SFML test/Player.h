#pragma once

#include "LibsAndDeclarations.h"

class Player
{
public:
	Player();
	Player(const Player&) = delete;
	Player& operator=(const Player&) = delete;
	~Player();

private:
	const float speed;
	sf::RectangleShape shape;

public:
	bool init();
	bool update(float dt);
	void draw(sf::RenderWindow &window);
	void release();

	sf::Vector2f position() { return shape.getPosition(); }
};
